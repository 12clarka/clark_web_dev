<!DOCTYPE html>
<html lang=en>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="stylesheet.css">
	<title>Home</title>
</head>

<body>
	<h1>Simpson Computer Science Club</h1>
	<h2>Home</h2>
	<ul>
		<li><a href="about.php">About Us</a></li>
		<li><a href="activities.php">Club Activities</a></li>
		<li><a href="contests.php">Programming Contests</a></li>
	</ul>
	<img src="images/group.jpg">
</body>
</html>