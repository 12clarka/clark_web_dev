<?php
session_start();
 
$error=FALSE;
 
if(empty($_REQUEST['username'])) {
    $error = TRUE;
} else {
    $username = $_REQUEST['username'];
    if (!preg_match("/^[A-Za-z]{1,25}$/", $username)) {
        $error = TRUE;
    }
}
 
if(!$error) {
    include ("db_setup.php");
    $connection = mysqli_connect($server, $username1, $password, $database) or die("Unable to connect");

    $username_safe = mysqli_escape_string($connection, $username);
 
    $query = "select * from people where username = '$username_safe'";
 
    $result = mysqli_query($connection, $query) or die("Query failed");
 
    $row = mysqli_fetch_assoc($result);
 
    if($row) {
        $form['username'] = $row['username'];
        $form['first_name'] = $row['first_name'];
        $form['last_name'] = $row['last_name'];
    } else {
        die("That record doesn't exist.");
    }
 
    $_SESSION['form'] = $form;
 
    $messages['message'] = "Editing the record for " . $form['first_name'] . " " . $form['last_name'];
 
    $_SESSION['messages'] = $messages;
 
    header("Location: form.php");
} else {
    header("Location: list_people.php");
}
?>