<!DOCTYPE html>
 
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Sample PHP</title>
</head>
 
<body>
  <?php
$error = FALSE;

  if(empty($_REQUEST['firstname'])) {
          $messages['firstname']="<p class='errormsg'>Error - Invalid First Name</p>";
          $error=TRUE;;
  } else {
          $firstname = $_REQUEST['firstname'];
          if (!preg_match("/^[A-Za-z]{1,25}$/", $firstname)) {
          		 $error=TRUE;
                 $messages['firstname']="<p class='errormsg'>Error - Invalid First Name</p>";
          }
  }

    if(empty($_REQUEST['last_name'])) {
          $messages['last_name']="<p class='errormsg'>Error - Invalid Last Name</p>";
          $error=TRUE;
  } else {
          $last_name = $_REQUEST['last_name'];
          if (!preg_match("/^[A-Za-z]{1,25}$/", $last_name)) {
          		 $error=TRUE;
                 $messages['last_name']="<p class='errormsg'>Error - Invalid Last Name</p>";
          }
  }

      if(empty($_REQUEST['username'])) {
          $messages['username']="<p class='errormsg'>Error - Invalid Username</p>";
          $error=TRUE;
  } else {
          $username = $_REQUEST['username'];
          if (!preg_match("/^[A-Za-z]{1,25}$/", $username)) {
          		 $error=TRUE;
                 $messages['username']="<p class='errormsg'>Error - Invalid Username</p>";
          }
  }


    if(empty($_REQUEST['email'])) {
          $messages['email']="<p class='errormsg'>Error - Invalid Email</p>";
          $error=TRUE;
  } else {
          $email = $_REQUEST['email'];
          if (!preg_match("/^\S+@\S+\.\S+$/", $email)) {
          		 $error=TRUE;
                 $messages['email']="<p class='errormsg'>Error - Invalid Email</p>";
          }
  }

    if(empty($_REQUEST['password'])) {
          $messages['password']="<p class='errormsg'>Error - Invalid Password</p>";
          $error=TRUE;
  } else {
          $password = $_REQUEST['password'];    
  }

    if(empty($_REQUEST['password_repeat'])) {
          $messages['password_repeat']="<p class='errormsg'>Error - Please repeat password</p>";
          $error=TRUE;
          }
     else {
     	  $password_repeat = $_REQUEST['password_repeat'];
     	  if ($password_repeat != $password)
	      {
	      	$error = TRUE;
	      	 $messages['password_repeat']="<p class='errormsg'>Error - Passwords must match</p>";
	      }

     }

    if(empty($_REQUEST['sex'])) {
          $messages['sex']="<p class='errormsg'>Error - Invalid Sex</p>";
          $error=TRUE;;
  } else {
          $sex = $_REQUEST['sex'];
          if(!preg_match("/^(Male|Female|Blank)$/", $sex))
          {
          	$error = TRUE;
          	$messages['sex']="<p class='errormsg'>Error - Invalid Sex</p>";
     	  }
     	}
  

    if(empty($_REQUEST['date_of_birth'])) {
          $error=TRUE;
          $messages['date_of_birth']="<p class='errormsg'>Error - Invalid Date of Birth</p>";
  } else {
          $date_of_birth = $_REQUEST['date_of_birth'];
          }
 

  if($error) {
    include("reg_form.php");
} else {
    $username2 = "clark_admin";
    $password2 = "mLRZu5wasKMbnx8K";
    $database = "clark_admin";
    $server   = "127.0.0.1";
  
    $connection = mysqli_connect($server, $username2, $password2, $database) or die("Unable to connect");
    $first_name_safe = mysqli_escape_string($connection, $firstname);
    $last_name_safe = mysqli_escape_string($connection, $last_name);
    $username_safe = mysqli_escape_string($connection, $username);
    $email_safe = mysqli_escape_string($connection, $email);
    $password_safe = mysqli_escape_string($connection, $password);
    $gender_safe = mysqli_escape_string($connection, $sex);
    $date_of_birth_safe = mysqli_escape_string($connection, $date_of_birth);
    $x = getdate();
    $join_date_safe= $x['year'] . '-' . $x['mon'] . '-' . $x['mday'];

    $query="insert into users (first_name, last_name, username, password, email, date_of_birth, gender, join_date) values ('$first_name_safe', '$last_name_safe', '$username_safe', '$password_safe', '$email_safe', '$date_of_birth_safe', '$gender_safe', '$join_date_safe')";

    mysqli_query($connection, $query) or die("Insert query failed to run.");
    mysqli_close($connection);

     $formdata = array(
      'first_name' => $first_name_safe,
      'last name' => $last_name_safe,
      'username' => $username_safe,
      'email' => $email_safe,
      'password' => $password_safe,  
      'gender' => $gender_safe,  
      'date_of_birth' => $date_of_birth_safe,
      'join_date' => $join_date_safe
      );
     $jsondata = json_encode($formdata, JSON_PRETTY_PRINT);
     if(file_put_contents("dirdata/formdata.txt", $jsondata)) echo "Data successfully saved";
     else echo "Unable to save data";

    include("next.php");
    include("list_user.php");
}
  ?>
  </body>
  </html>