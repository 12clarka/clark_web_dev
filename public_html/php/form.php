<!DOCTYPE html>
<html lang="en">
<head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Sample Form</title>
</head>
<body>
	<h1>Form Test</h1>
	<form action="form_process.php" method="post" autocomplete="off">
		<label for ="firstname">First Name: </label>
		<input id="firstname" name="firstname" type="text" placeholder="Tony"/>
		<br />
		<label for ="Last Name">Last Name: </label>
		<input id="Last Name" name="Last Name" type="text" placeholder="Clark"/>
		<br />
		<label for ="Password">Password: </label>
		<input id="Password" name="Password" type="password" placeholder="iliketurtles"/>
		<br />

		<fieldset>
			<input type="radio" name="gender" value="male" /> Male<br />
			<input type="radio" name="gender" value="Female" /> Female<br />
			<input type="radio" name="gender" value="Dog" /> Dog<br />
		</fieldset>

		<fieldset>
			I like turtles: <input type="checkbox" name="turtles[]" value"Turtles" /><br />
			I don't like turtles: <input type="checkbox" name="turtles[]" value"No Turtles" /><br />
			Warble Flarble: <input type="checkbox" name="turtles[]" value"Warble Flarble" /><br />
		</fieldset>

		<textarea name = "mymessage" rows="3" cols="40" placeholder="I'm going off the rails on a crazy train"></textarea>

		<!-- Submit Button-->
		<input type="submit" value="OK"/>


	</form>
</body>
</html>