<!DOCTYPE html>
<html lang=en>
<head>
	<meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="stylesheet.css">
	<title>Contests</title>
</head>
<body>
	<h1>Simpson Computer Science Club</h1>
	<h2>Activities</h2>
	<ul>
		<li><a href="index.php">Home</a></li>
		<li><a href="about.php">About Us</a></li>
		<li><a href="activities.php">Activities</a></li>
	</ul>
	<p>The club travels to two programming contests each year. <br> 
	In the Fall Semester we participate in the ACM Programming competion, 
	and in the Spring Semester we participate in the MICS conference.</p>
	
	<!-- Table of Contests -->
<table>
  <thead>
    <tr>
      <th>Contest</th>
      <th>Date</th>
      <th>Website</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>ACM Contest</td>
      <td>November 8, 2014</td>
      <td><a href="http://ncna-region.unl.edu/">http://ncna-region.unl.edu/</a>
  </tbody>
  <tbody>
    <tr>
      <td>MICS Conference</td>
      <td>April 10-11, 2015</td>
      <td><a href="http://www.micsymposium.org/mics2015/">http://www.micsymposium.org/mics2015/</a></td>
    </tr>
  </tbody>
</table>
</body>
</html>