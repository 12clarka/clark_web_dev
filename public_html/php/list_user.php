<!DOCTYPE html>
  
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <title>List People</title>
  <script type="text/javascript">
  $(document).ready(function() {
	$('a.delete').click(function(e) {
		e.preventDefault();
		var parent = $(this).parent().parent();
		$.ajax({
			type: 'get',
			url: 'delete_user.php',
			data: 'username=' + parent.attr('id').replace('record-',''),
			beforeSend: function() {
				parent.animate({'backgroundColor':'#fb6c6c'},{queue: false, duration: 300, easing: 'swing'});
			},
			success: function() {
				parent.slideUp(300,function() {
					parent.remove();
				});
			}
		});
	});
});
  </script>
</head>
  
<body>
<div class="container">
  <h1>List of people:</h1>
      <table class="table table-bordered"> 
        <thread>
            <tr class = "danger">
              <td>Name</td>
              <td>Username</td>
              <td>Email</td>
              <td>Password</td>
              <td>Gender</td>
              <td>Date of Birth</td>
              <td>Join Date</td>
              <td>Delete</td>
              <td>Edit</td>
            </tr>
  <?php
  /* Info to hook up to a database.
   * Normally, we'd import this from a common file. You would
   * not want to update this in each file when the password
   * changes.
   */
  $username = "clark_admin";
  $password = "mLRZu5wasKMbnx8K";
  $database = "clark_admin";
  $server   = "127.0.0.1";
  
  $connection = mysqli_connect($server, $username, $password, $database) or die("Unable to connect");
 
  // Pull all users from the database
  $query = "SELECT * FROM users";
  $result = mysqli_query($connection, $query) or die("Query failed");
  
  // Loop through each record. This could also
  // be done with a 'for' loop.
  while($row = mysqli_fetch_assoc($result)) 
  {
    $first_name = htmlentities($row['first_name']);
    $last_name = htmlentities($row['last_name']);
    $username = htmlentities($row['username']);
    $email = htmlentities($row['email']);
    $pass = htmlentities($row['password']);
    $gender = htmlentities($row['gender']);
    $date_of_birth = htmlentities($row['date_of_birth']);
    $join_date = htmlentities($row['join_date']);

            echo '<tr class="active" id="record-',$row['username'],'">';          
              echo "<td>", $first_name . ' ' . $last_name, "</td>";
              echo "<td>", $username, "</td>";
              echo "<td>", $email, "</td>";
              echo "<td>", $pass, "</td>";
              echo "<td>", $gender, "</td>";
              echo "<td>", $date_of_birth, "</td>";
              echo "<td>", $join_date, "</td>";
              echo '<td><a href="?delete=',$row['username'],'" class="delete"><button type="button" class="btn btn-default">Delete</button></a> </td>';
              echo '<td><a href="edit_user.php?username=$username"><button type="button" class="btn btn-default">Edit</button></a> </td>';
            "</tr>";
  }
    "</thread>";
    "</table>";
  mysqli_free_result($result);
  mysqli_close($connection);
  ?>
  </div>
</body>
</html>