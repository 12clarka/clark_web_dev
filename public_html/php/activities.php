<!DOCTYPE html>
<html lang=en>
<head>
	<meta charset="utf-8">

	<title>Activities</title>
</head>
<body>
	<h1>Simpson Computer Science Club</h1>
	<h2>Activities</h2>
	<ul>
		<li><a href="index.php">Home</a></li>
		<li><a href="about.php">About Us</a></li>
		<li><a href="contests.php">Programming Contests</a></li>
	</ul>
	<p>The club participates in various activities such as</p>
	<ul>
		<li>Computer Building</li>
		<li>Science Off Trivia Contest</li>
		<li>Robotics</li>
		<li>Hackathons</li>
		<li>Job Crawls</li>
		<li>Guest Speakers</li>
	</ul>
</body>
</html>