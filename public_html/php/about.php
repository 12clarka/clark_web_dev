<!DOCTYPE html>
<html lang=en>
<head>
	<meta charset="utf-8">

	<title>About</title>
</head>
<body>
<?php
	include("header.php");
	writeHead('about.php');
	include("menu.php");
?>
	<p>The Simpson Computer Science Club was started in 2013 to promote 
		interest in the are of computer science, and hosts activities
		to increase skills and knowledge when working with computers.</p>
	<br>
	<p>Current officers:</p>
	<p>President:  Kendra Klocke<br>
	Vice President:  Thomas Klein<br>
	Secretary:  Josh Sutton<br>
	Treasurer:  Tony Clark<br>
	Public Relations:  Ellie Leube</p>
</body>
</html>