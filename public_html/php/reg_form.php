<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Register</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class = "container">
	<h1>Registration Form</h1>
	<form action="form_process.php" method="get" autocomplete="off" class = "form-horizontal">

	<table>
	<?php 
	  session_start();
  	if(isset($_SESSION['messages'])) 
  	{
	    $messages = $_SESSION['messages'];
	    $form = $_SESSION['form'];
	    unset($_SESSION['messages']);
	    unset($_SESSION['form']);
 	} 
	 if(!empty($messages)) 
	 {
      	 foreach ($messages as $message) 
      	 {
         	echo $message;
      	 }
  	 }
	?>
			<tr>
				<td><label for ="firstname">First Name: </label></td>
				<td><input id="firstname" name="firstname" <?php
				if( isset($firstname) ) {
			        echo 'value="';
			        echo htmlentities ($firstname);
			        echo '"';
			    }
			        ?>type="text" placeholder="First Name"/></td>
				 
			    
			 	
			</tr>
			<tr>
				<td><label for ="last_name">Last Name: </label></td>
				<td><input id="last_name" name="last_name" <?php
			     if( isset($last_name) ) {
			        echo 'value="';
			        echo htmlentities ($last_name);
			        echo '"';
			    }
			        ?> type="text" placeholder="Last Name"/></td>
				 

			</tr>
			<tr>
				<td><label for ="username">Username: </label></td>
				<td><input id="last_name" name="username" <?php
			     if( isset($username) ) {
			        echo 'value="';
			        echo htmlentities ($username);
			        echo '"';
			    }
			        ?> type="text" placeholder="Username"/></td>
				 

			</tr>
			<tr>
				<td><label for ="email">Email Address: </label></td>
				<td><input id="email" name="email" <?php
			     if( isset($email) ) {
			        echo 'value="';
			        echo htmlentities ($email);
			        echo '"';
			    }
			        ?> type="text" placeholder="firstlast@gmail.com"/> </td>

			</tr>
			<tr>
				<td><label for ="password">Password: </label></td>
				<td><input id="Password" name="password" <?php
				if( isset($password) ) {
			        echo 'value="';
			        echo htmlentities ($password);
			        echo '"';
			    }
			        ?> type="password" placeholder="*********"/></td>
				 
			     
			</tr>
			<tr>
				<td><label for ="password_repeat">Repeat Passoword: </label></td>
				<td><input id="password_repeat" name="password_repeat" type="password" placeholder="*********"/></td>
				 
			</tr>
	</table>

	<input type="radio" name="sex" value="Male" <?php if( isset($sex) and $sex=="Male" ) {echo "checked ";} ?>>Male
	<br>
	<input type="radio" name="sex" value="Female" <?php if( isset($sex) and $sex=="Female" ) {echo "checked ";} ?>>Female
	<br />
	<input type ="radio" name="sex" value="Blank" <?php if( isset($sex) and $sex=="Blank" ) {echo "checked ";} ?>>I don't wish to answer
	<br />
	Birthday:
  	<input type="date" name="date_of_birth" <?php if( isset($date_of_birth)){ echo 'value="'; echo htmlentities($date_of_birth); echo '"';} ?>>

	<input type="submit" value="Submit"/>
	</form>
	</div>
</body>
</html>